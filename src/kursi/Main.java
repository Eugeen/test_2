package kursi;

import kursi.TEST.Test_N_1;
import kursi.TEST.Test_N_2;

public class Main {

    public static void main(String[] args)   {
        Test_N_1 test = new Test_N_1();
        test.go();
        Test_N_2 test_2 = new Test_N_2();
        test_2.go();
    }
}
