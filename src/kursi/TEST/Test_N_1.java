package kursi.TEST;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Test_N_1 {
    public void go() {

        String FILE_NAME = "D:\\test.txt";

        Set<String> set = null;
        try {
            set = Files.lines(Paths.get(FILE_NAME), Charset.forName("CP1251")).collect(Collectors.toSet());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Set<Person> personSet = new TreeSet<>();
        List<Person> persons = new ArrayList<>();
        for (String s : set) {
            Person person = new Person();
            person.setName(s.split(" ")[0]);
            person.setSurname(s.split(" ")[1]);
            person.setAge(Integer.parseInt(s.split(" ")[2]));
            personSet.add(person);
            persons.add(person);
        }
        System.out.println(personSet);
        List<Person> randompersons = randomsort (persons);
        System.out.println(randompersons);

    }

    public List<Person> randomsort(List<Person> people){
        Random random = new Random();
        List<Person> p = new ArrayList<>();
        for (int i = 0; i < people.size(); i++) {
            String name = people.get(random.nextInt(people.size())).getName();
            String surname = people.get(random.nextInt(people.size())).getSurname();
            int age = people.get(random.nextInt(people.size())).getAge();
            Person person = new Person(name,surname,age);
            p.add(person);
        }
        return p;
    }

}
