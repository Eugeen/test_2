package kursi.TEST;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Test_N_2 {
    public void go() {
        String FILE_NAME = "D:\\test2.txt";
        List<String> list = null;
        try {
            list = Files.lines(Paths.get(FILE_NAME), Charset.forName("CP1251")).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> newlist = new ArrayList<>();
        for (String s : list) {
            if (s.length() > 100) {
                String newStr = s.replaceAll("(.{100})", "$1|");
                String[] newStrings = newStr.split("\\|");
                for (String ss : newStrings) {
                    newlist.add(ss);
                }
            } else newlist.add(s);
        }
        for (String s : newlist) {
            System.out.println(s);
        }
    }
}
